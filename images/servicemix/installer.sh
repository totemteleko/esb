# start in background
./servicemix/bin/start

# run your commands 

list=$(echo "$features" | sed -e 's/,/\n/g')
echo "features to be installed:"$list
sleep 5s

./servicemix/bin/client -u smx -p smx "features:install camel-jetty; osgi:shutdown -f"
while [ "$?" != "0" ]; do
	sleep 5s
	./servicemix/bin/client -u smx -p smx "features:install $(echo $list); osgi:shutdown -f"
done

echo "done"
exit 0
